CREATE TABLE "public"."experienceLevel"("experienceLevel" text NOT NULL, PRIMARY KEY ("experienceLevel") );

INSERT INTO "experienceLevel" ("experienceLevel") VALUES ('beginner'), ('medium'), ('pro');
