CREATE TABLE "public"."timeCommitmentTypes"("value" text NOT NULL, PRIMARY KEY ("value") );

INSERT INTO "timeCommitmentTypes" ("value") VALUES ('hours'), ('days'), ('weeks'), ('months');
