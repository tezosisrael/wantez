CREATE TABLE "public"."category"("category" text NOT NULL, PRIMARY KEY ("category") );

INSERT INTO "category" (category) VALUES ('frontend'), ('backend'), ('design'), ('docs'), ('other');

