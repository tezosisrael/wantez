# Wantez

## Development

- set gitlab ssh keys

- install deps: yarn, nodejs, docker-compose, vercel

- run the following commands:

```
yarn
vercel env pull ./web/.env.local
yarn db:create
yarn dev
```

### Commit

We adhere to the basic rules of commitlint: https://www.npmjs.com/package/@commitlint/config-conventional
